﻿using UnityEngine;
using MySql.Data.MySqlClient;

namespace MySQLUtils
{
    public class MySQL_Handler
    {
        public  MySqlConnection connection { get; set; }

        /// <summary>
        /// Connects to a database.
        /// Should not be used outside MySQL_Utils, use MySQL_Utils.Connect() instead
        /// </summary>
        /// <param name="host"></param>
        /// <param name="database"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool Connect(string host, string database, string user, string password, int port)
        {
            string connString;
            if (port == 0)
            {
                if(database == "")
                    connString = $"SERVER={host};UID={user};PASSWORD={password};PORT=3306";
                else
                    connString = $"SERVER={host};DATABASE={database};UID={user};PASSWORD={password};PORT=3306";
            }
            else
            {
                if(database == "")
                    connString = $"SERVER={host};UID={user};PASSWORD={password};PORT={port}";
                else
                    connString = $"SERVER={host};DATABASE={database};UID={user};PASSWORD={password};PORT={port}";
            }

            try {
                connection = new MySqlConnection(connString);
                connection.Open();
                return true;
            } catch (MySqlException ex)
            {
                switch(ex.Number)
                {
                    case 0:
                        Debug.LogWarning("Error connecting to the database: couldn't connect to server");
                        break;
                    case 1:
                        Debug.LogWarning("Error connecting to the database: access denied to " + user + "@" + host);
                        break;
                    default:
                        Debug.LogWarning("Error connecting to the database: Unknown error(" + ex.Number + ")");
                        break;
                }
                return false;
            }
        }
    }
}

