﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;

namespace MySQLUtils
{
    public class MySQL_Utils
    {
        private string currTable;
        private MySqlConnection DB;
        private MySQL_Handler handler;
        private string _host { get; set; }
        private int _port { get; set; }
        private string _user { get; set; }
        private string _password { get; set; }
        private string _database { get; set; }

        public MySQL_Utils(string host, string database, string user, string password)
        {
            handler = new MySQL_Handler();
            Assign(host, database, user, password);
        }

        public MySQL_Utils(string host,string user, string password)
        {
            handler = new MySQL_Handler();
            Assign(host, user, password);
        }

        public MySQL_Utils(string host, string database, string user, string password, int port)
        {
            handler = new MySQL_Handler();
            Assign(host, database, user, password, port);
        }

        public MySQL_Utils(string host, string user, string password, int port)
        {
            handler = new MySQL_Handler();
            Assign(host, user, password, port);
        }

        /// <summary>
        /// Connects to Database
        /// </summary>
        /// <returns></returns>
        public bool Connect()
        {
            if(handler != null)
            {
                bool connected = handler.Connect(_host, _database, _user, _password, _port);
                if (connected)
                {
                    DB = handler.connection;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Closes active connection to a database.
        /// </summary>
        public void Close()
        {
            DB.Close();
        }

        public void SelectDB(string db)
        {
            CleanConnection();
            currTable = db;
            DB.ChangeDatabase(db);
        }

        private void Assign(string host, string database, string user, string password)
        {
            this._host = host;
            this._database = database;
            this._user = user;
            this._password = password;
        }

        private void Assign(string host, string database, string user, string password, int port)
        {
            this._host = host;
            this._database = database;
            this._user = user;
            this._password = password;
            this._port = port;
        }

        private void Assign(string host, string user, string password)
        {
            this._host = host;
            this._user = user;
            this._password = password;
        }

        private void Assign(string host, string user, string password, int port)
        {
            this._host = host;
            this._user = user;
            this._password = password;
            this._port = port;
        }

        private void CleanConnection()
        {
            if (DB.State != System.Data.ConnectionState.Open)
            {
                DB.Close();
                DB.Open();
            }
        }

        /// <summary>
        /// Updates a table in the database
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="table"></param>
        /// <param name="condition"></param>
        /// <param name="limit"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool Update(string[] rows, string table, Dictionary<string,string> condition, int[] limit, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();
            
            try
            {
                string query = "UPDATE " + table +" SET ";
                for (int i = 0; i < rows.Length; i++)
                {
                    if(i + 1 != rows.Length)
                        query += rows[i] + "=?" + rows[i] + ",";
                    else
                        query += rows[i] + "=?" + rows[i];
                }
                if(condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach (KeyValuePair<string,string> c in condition)
                    {
                        if (cc == 0)
                            query += " WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;

                        cc++;
                    }
                }
                if (limit != null)
                {
                    if (limit.Length == 1)
                        query += " LIMIT " + limit[0];
                    else
                        query += " LIMIT " + limit[0] + ", " + limit[1];
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a table in the database
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="condition"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool Update(string[] rows, Dictionary<string, string> condition, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            if (currTable == "")
            {
                UnityEngine.Debug.LogWarning("No database selected, please select it or specify it");
                return false;
            }

            try
            {
                string query = "UPDATE " + currTable + " SET ";
                for (int i = 0; i < rows.Length; i++)
                {
                    if (i + 1 != rows.Length)
                        query += rows[i] + "=?" + rows[i] + ",";
                    else
                        query += rows[i] + "=?" + rows[i];
                }
                if (condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach (KeyValuePair<string, string> c in condition)
                    {
                        if (cc == 0)
                            query += " WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;

                        cc++;
                    }
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a table in the database
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="condition"></param>
        /// <param name="limit"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool Update(string[] rows, Dictionary<string, string> condition, int[] limit, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            if (currTable == "")
            {
                UnityEngine.Debug.LogWarning("No database selected, please select it or specify it");
                return false;
            }

            try
            {
                string query = "UPDATE " + currTable + " SET ";
                for (int i = 0; i < rows.Length; i++)
                {
                    if (i + 1 != rows.Length)
                        query += rows[i] + "=?" + rows[i] + ",";
                    else
                        query += rows[i] + "=?" + rows[i];
                }
                if (condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach (KeyValuePair<string, string> c in condition)
                    {
                        if (cc == 0)
                            query += " WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;

                        cc++;
                    }
                }
                if (limit != null)
                {
                    if (limit.Length == 1)
                        query += " LIMIT " + limit[0];
                    else
                        query += " LIMIT " + limit[0] + ", " + limit[1];
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Updates a table in the database
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="table"></param>
        /// <param name="condition"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool Update(string[] rows, string table, Dictionary<string, string> condition, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            try
            {
                string query = "UPDATE " + table + " SET ";
                for (int i = 0; i < rows.Length; i++)
                {
                    if (i + 1 != rows.Length)
                        query += rows[i] + "=?" + rows[i] + ",";
                    else
                        query += rows[i] + "=?" + rows[i];
                }
                if (condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach (KeyValuePair<string, string> c in condition)
                    {
                        if (cc == 0)
                            query += " WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;

                        cc++;
                    }
                }
               
                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.ExecuteNonQuery();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Selects and returns a datatable from database
        /// </summary>
        /// <param name="select"></param>
        /// <param name="limit"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Select(string[] select, int[] limit, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            if (currTable == "")
            {
                UnityEngine.Debug.LogWarning("No database selected, please select it or specify it");
                return null;
            }

            try
            {
                string query = "SELECT ";
                for (int s = 0; s < select.Length; s++)
                {
                    if (s + 1 == select.Length)
                        query += select[s];
                    else
                        query += select[s] + ",";
                }
                query += " FROM " + currTable + " ";
                if (limit != null)
                {
                    if (limit.Length == 1)
                        query += " LIMIT " + limit[0];
                    else
                        query += " LIMIT " + limit[0] + ", " + limit[1];
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    da.Fill(result);
            }
            catch (MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }
            return result;
        }

        /// <summary>
        /// Selects and returns a datatable from database
        /// </summary>
        /// <param name="select"></param>
        /// <param name="table"></param>
        /// <param name="limit"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Select(string[] select, string table, int[] limit, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            try
            {
                string query = "SELECT ";
                for (int s = 0; s < select.Length; s++)
                {
                    if (s + 1 == select.Length)
                        query += select[s];
                    else
                        query += select[s] + ",";
                }
                query += " FROM " + table + " ";
                if (limit != null)
                {
                    if (limit.Length == 1)
                        query += " LIMIT " + limit[0];
                    else
                        query += " LIMIT " + limit[0] + ", " + limit[1];
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    da.Fill(result);
            }
            catch (MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }
            return result;
        }

        /// <summary>
        /// Selects and returns a datatable from database
        /// </summary>
        /// <param name="select"></param>
        /// <param name="table"></param>
        /// <param name="condition"></param>
        /// <param name="limit"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Select(string[] select, string table, Dictionary<string,string> condition, int[] limit, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            try
            {
                string query = "SELECT ";
                for (int s = 0; s < select.Length; s++)
                {
                    if (s + 1 == select.Length)
                        query += select[s];
                    else
                        query += select[s] + ",";
                }
                query += " FROM " + table + " ";
                if(condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach(KeyValuePair<string,string> c in condition)
                    {
                        if (cc == 0)
                            query += "WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;
                        cc++;
                    }
                }
                if(limit != null)
                {
                    if (limit.Length == 1)
                        query += " LIMIT " + limit[0];
                    else
                        query += " LIMIT " + limit[0] + ", " + limit[1];
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    da.Fill(result);
            }
            catch(MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }
            return result;
        }

        /// <summary>
        /// Selects and returns a datatable from database
        /// </summary>
        /// <param name="select"></param>
        /// <param name="table"></param>
        /// <param name="condition"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Select(string[] select, string table, Dictionary<string, string> condition, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            try
            {
                string query = "SELECT ";
                for (int s = 0; s < select.Length; s++)
                {
                    if (s + 1 == select.Length)
                        query += select[s];
                    else
                        query += select[s] + ",";
                }
                query += " FROM " + table + " ";
                if (condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach (KeyValuePair<string, string> c in condition)
                    {
                        if (cc == 0)
                            query += "WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;
                        cc++;
                    }
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    da.Fill(result);
            }
            catch (MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }
            return result;
        }

        /// <summary>
        /// Selects and returns a datatable from database
        /// </summary>
        /// <param name="select"></param>
        /// <param name="condition"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Select(string[] select, Dictionary<string, string> condition, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            if (currTable == "")
            {
                UnityEngine.Debug.LogWarning("No database selected, please select it or specify it");
                return null;
            }

            try
            {
                string query = "SELECT ";
                for (int s = 0; s < select.Length; s++)
                {
                    if (s + 1 == select.Length)
                        query += select[s];
                    else
                        query += select[s] + ",";
                }
                query += " FROM " + currTable + " ";
                if (condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach (KeyValuePair<string, string> c in condition)
                    {
                        if (cc == 0)
                            query += "WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;
                        cc++;
                    }
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    da.Fill(result);
            }
            catch (MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }
            return result;
        }

        /// <summary>
        /// Select and returns a table from database
        /// </summary>
        /// <param name="select"></param>
        /// <param name="condition"></param>
        /// <param name="limit"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Select(string[] select, Dictionary<string, string> condition, int[] limit, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();

            if(currTable == "")
            {
                UnityEngine.Debug.LogWarning("No database selected, please select it or specify it");
                return null;
            }

            try
            {
                string query = "SELECT ";
                for (int s = 0; s < select.Length; s++)
                {
                    if (s + 1 == select.Length)
                        query += select[s];
                    else
                        query += select[s] + ",";
                }
                query += " FROM " + currTable + " ";
                if (condition != null || condition.Count > 0)
                {
                    int cc = 0;
                    foreach (KeyValuePair<string, string> c in condition)
                    {
                        if (cc == 0)
                            query += "WHERE " + c.Key;
                        else
                            query += " " + c.Value + " " + c.Key;
                        cc++;
                    }
                }
                if (limit != null)
                {
                    if (limit.Length == 1)
                        query += " LIMIT " + limit[0];
                    else
                        query += " LIMIT " + limit[0] + ", " + limit[1];
                }

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    da.Fill(result);
            }
            catch (MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }
            return result;
        }

        /// <summary>
        /// Insert query with list of parameters.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="rows"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool Insert(string table, string[] rows, List<MySqlParameter> parameters)
        {
            CleanConnection();
            DataTable result = new DataTable();
            
            try
            {
                string query = "INSERT INTO " + table + " (";
                int rc = 1;
                foreach (string r in rows)
                {
                    if (rc != rows.Length)
                        query += r + ",";
                    else
                        query += r;

                    rc++;
                }
                query += ") VALUES (";
                int rp = 1;
                foreach (string p in rows)
                {
                    if (rp != rows.Length)
                        query += "?" + p + ",";
                    else
                        query += "?" + p;

                    rp++;
                }
                query += ")";

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.ExecuteNonQuery();
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Insert query with list of parameters.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool Insert(string[] rows, List<MySqlParameter> parameters)
        {
            if(currTable == "")
            {
                UnityEngine.Debug.LogWarning("No database selected, please specify it or selected it before running a query.");
                return false;
            }

            CleanConnection();
            DataTable result = new DataTable();

            try
            {
                string query = "INSERT INTO " + currTable + " (";
                int rc = 1;
                foreach (string r in rows)
                {
                    if (rc != rows.Length)
                        query += r + ",";
                    else
                        query += r;

                    rc++;
                }
                query += ") VALUES (";
                int rp = 1;
                foreach (string p in rows)
                {
                    if (rp != rows.Length)
                        query += "?" + p + ",";
                    else
                        query += "?" + p;

                    rp++;
                }
                query += ")";

                MySqlCommand cmd = new MySqlCommand(query, DB);
                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.ExecuteNonQuery();

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Selects all the data in a table, results can be limited
        /// </summary>
        /// <param name="table"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public DataTable SelectAll(string table, string limit = "")
        {
            CleanConnection();
            DataTable result = new DataTable();
            string query = $"SELECT * FROM {table} {limit}";

            try
            {
                MySqlCommand cmd = new MySqlCommand(query, DB);
                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    da.Fill(result);

                return result;
            }
            catch (MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Run a non specific query
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <param name="fetch"></param>
        /// <returns></returns>
        public DataTable Query(string query, List<MySqlParameter> parameters, bool fetch)
        {
            CleanConnection();
            DataTable result = new DataTable();
            
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, DB);

                foreach (MySqlParameter param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                if (fetch)
                {
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                        da.Fill(result);
                }
                else
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (MySqlException ex)
            {
                UnityEngine.Debug.LogError(ex.Message);
                return null;
            }

            return result;
        }
    }
}
