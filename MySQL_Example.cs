﻿using UnityEngine;
using System.Data;
using System.Collections.Generic;

//Mysql
using MySql.Data.MySqlClient;
using MySQLUtils;

public class MySQL_Example : MonoBehaviour
{
    // Table used in the following examples (included in the asset pack):

    /*
     * 
     *      id      user    value
     *      
     *      3        4       321
     *      2        35      92
     *      1        32      45
     *      0        34      322
     * 
     * 
     */


    private MySQL_Utils mysql;

    void Start()
    {
        //Prepare the connection
        //IMPORTANT: don't leave this kind of information here, make some request to a remote api to get the password for e.g.
        mysql = new MySQL_Utils("localhost", "my_db", "my_user", "my_password");
        //mysql = new MySQL_Utils("localhost", "my_user", "my_password");
        //mysql = new MySQL_Utils("localhost", "my_user", "my_password", 3309);

        //Connect 
        if (mysql.Connect())
        {
            mysql.SelectDB("my_db"); //Select a database

            //Some examples
            SelectAll();
            Insert();
            Select();
            _Update();
            Query();

            mysql.Close(); //Closes the connection to the database
        }
    }

    private void Query()
    {
        List<MySqlParameter> parameters = new List<MySqlParameter>(){ MySQL_Param.Parameter("?id", 6) };
        DataTable result = mysql.Query("SELECT * FROM highscore WHERE user!=0 AND id=?id LIMIT 1", parameters, true);
    }

    // Select all from table "highscore" and log some value
    private void SelectAll()
    {
        //Run query
        DataTable result = mysql.SelectAll("highscore");

        //Extract what we want from the result DataTable
        DataRow[] rows = result.Select("id=2");

        //Log
        foreach (DataRow row in rows)
        {
            Debug.Log("Value of id 2: " + row["value"]);
        }
    }

    //Insert into table "highscore"
    private void Insert()
    {
        //Parameters
        List<MySqlParameter> parameters = new List<MySqlParameter>();
        string[] iRows = new string[] { "user", "value" };
        parameters.Add(MySQL_Param.Parameter(iRows[0], 54));
        parameters.Add(MySQL_Param.Parameter(iRows[1], "333"));

        //Run the query
        mysql.Insert("highscore", iRows, parameters);
    }

    //Select passing in a query
    private void Select()
    {
        //Parameters
        List<MySqlParameter> selectParams = new List<MySqlParameter>();
        string[] s = new string[] { "user", "value" };
        selectParams.Add(MySQL_Param.Parameter("?value", 120));

        //Conditions
        // The dictionary key contains the comparision and the value contains 'AND' or 'OR'
        // Passing in a value in thhe first dictionary key will be ignored
        Dictionary<string, string> conditions = new Dictionary<string, string>();
        conditions.Add("value>?value", "");
        conditions.Add("id!=0", "AND");

        //Run the query
        DataTable selectResult = mysql.Select(s, "highscore", conditions, new int[] { 0, 10 }, selectParams);

        //Log
        Debug.Log("Users where value is bigger than 120 by descending order of ids: ");
        foreach (DataRow row in selectResult.Rows)
        {
            Debug.Log(row["user"]);
        }
    }

    //Update some value
    private void _Update()
    {
        //Rows
        string[] uRows = new string[] { "user", "value", "id" };

        //Parameters
        List<MySqlParameter> uParameters = new List<MySqlParameter>();
        uParameters.Add(MySQL_Param.Parameter(uRows[0], 1));
        uParameters.Add(MySQL_Param.Parameter(uRows[1], 1));
        uParameters.Add(MySQL_Param.Parameter(uRows[2], '3'));

        //Conditions
        Dictionary<string, string> uConditions = new Dictionary<string, string>();
        uConditions.Add("id=?id", "");

        //Run the query
        mysql.Update(uRows, "highscore", uConditions, uParameters);
    }
}
